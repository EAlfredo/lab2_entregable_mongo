package mis.entregables.lab2.consumo.rest.mongo.lab2.controlador;

import mis.entregables.lab2.consumo.rest.mongo.lab2.modelo.Producto;
import mis.entregables.lab2.consumo.rest.mongo.lab2.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping("/productos")
    public List<Producto> getProductos() {
        System.out.println("Busca productos en mongo");
        return servicioProducto.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<Producto> getProductoId(@PathVariable String id){
        System.out.println("Busca producto en mongo por ID");
        return servicioProducto.findById(id);
    }

    // agrego el metodo post ya probado el metodo get con nulos
    @PostMapping("/productos")
    public Producto postProductos(@RequestBody Producto newProducto){
        System.out.println("agrego producto en mongo");
        servicioProducto.save(newProducto);
        return newProducto;
    }

    // ahora le toca el metodo put
    @PutMapping("/productos")
    public void putProductos(@RequestBody Producto productoToUpdate){
        System.out.println("actualizo producto en mongo");
        servicioProducto.save(productoToUpdate);
    }

    // por ultimo en el LAB 2 entregables se agrega el metodo Delete
    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody Producto productoToDelete){
        System.out.println("se bora el producto en mongo");
        return servicioProducto.delete(productoToDelete);
    }
}
