package mis.entregables.lab2.consumo.rest.mongo.lab2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Consumorestmongolab2Application {

	public static void main(String[] args) {
		SpringApplication.run(Consumorestmongolab2Application.class, args);
	}

}
