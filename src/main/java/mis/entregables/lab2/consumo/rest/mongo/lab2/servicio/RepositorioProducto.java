package mis.entregables.lab2.consumo.rest.mongo.lab2.servicio;

import mis.entregables.lab2.consumo.rest.mongo.lab2.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioProducto extends MongoRepository<Producto, String> {
}

